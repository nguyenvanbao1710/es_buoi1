tinhTB = (...item) => {
  let num = 0;
  return (
    item.map((item) => {
      num += item;
    }),
    (num / item.length).toFixed(3)
  );
};

document.getElementById("btnKhoi1").onclick = () => {
  let toan = document.getElementById("inpToan").value * 1,
    ly = document.getElementById("inpLy").value * 1,
    hoa = document.getElementById("inpHoa").value * 1;
  document.getElementById("tbKhoi1").innerHTML = tinhTB(toan, ly, hoa);
};

document.getElementById("btnKhoi2").onclick = () => {
  let van = document.getElementById("inpVan").value * 1,
    su = document.getElementById("inpSu").value * 1,
    dia = document.getElementById("inpDia").value * 1,
    english = document.getElementById("inpEnglish").value * 1;
  document.getElementById("tbKhoi2").innerHTML = tinhTB(van, su, dia, english);
};
