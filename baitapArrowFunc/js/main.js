const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
let colorContainer = document.getElementById("colorContainer");
(loadColorButton = () => {
  for (let i = 0; i < colorList.length; i++)
    colorContainer.innerHTML +=
      0 == i
        ? "<button class='color-button " + colorList[i] + " active'></button>"
        : "<button class='color-button " + colorList[i] + "'></button>";
}),
  loadColorButton();
let btnColorPick = document.getElementsByClassName("color-button"),
  house = document.getElementById("house");
for (let i = 0; i < btnColorPick.length; i++) {
  btnColorPick[i].addEventListener("click", function () {
    changeColor(colorList[i], i);
  });
  changeColor = (a, j) => {
    for (let i = 0; i < btnColorPick.length; i++) {
      btnColorPick[i].classList.remove("active");
      btnColorPick[j].classList.add("active"), (house.className = "house " + a);
    }
  };
}
